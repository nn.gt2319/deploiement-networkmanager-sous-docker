FROM debian:bookworm

# Installation des paquets 
# Y compris quelques paquets utilitaires de debogage
RUN apt update -q --fix-missing \
    && apt upgrade -y \
    && DEBIAN_FRONTEND=noninteractive apt -y install --no-install-recommends \
    vim \
    nano \
    sudo \
    net-tools \
    dnsutils \
    wget \
    curl \
    nmap \
    tcpdump \
    git \
    mariadb-client \
    openssh-server \
    locales \
    apache2 \
    ssl-cert \
    ca-certificates \
    composer \
    apt-transport-https \
    gpg \
    php \
    php-xml \
    php-curl \
    php-twig \
    php-twig-i18n-extension \
    php-zip \
    php-tcpdf \
    php-webmozart-assert \
    php-symfony-yaml \
    php-symfony-var-exporter \
    php8.2-bz2 \
    php8.2-gd \
    php8.2-mysql \
    php8.2-zip \
    unzip \ 
    && apt clean \
    && rm -rf /var/lib/apt/lists/* \
    /var/log/alternatives.log \
    /var/log/apt/history.log \
    /var/log/apt/term.log \
    /var/log/dpkg.log \
    /tmp/* /var/tmp/*

# Configuration de la Timezone Europe/Paris et des locales 
ENV LANG=fr_FR.UTF-8  

RUN rm /etc/localtime \
    && ln -s /usr/share/zoneinfo/Europe/Paris /etc/localtime \
    && dpkg-reconfigure --frontend noninteractive tzdata \
    && sed -i 's/# fr_FR.UTF-8 UTF-8/fr_FR.UTF-8 UTF-8/' /etc/locale.gen \
    && echo 'LANG=${LANG}'>/etc/default/locale \
    && dpkg-reconfigure --frontend=noninteractive locales \
    && update-locale LANG=${LANG}

# Configuration du service SSH
# Creation d'un utilisateur "non root"
ENV SSHUSER user
ENV SSHPASS sshpass
RUN useradd -m -s /bin/bash -G sudo user \
 && echo "user:${SSHPASS}" | chpasswd

# Démarrage du service SSH
RUN service ssh start
EXPOSE 22

# Téléchargement du setup d’installation de composer
RUN wget -O composer-setup.php https://getcomposer.org/installer

# Installation de composer au niveau global (pour tous les utilisateurs) :
RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer

# Mise à jour de composer
RUN composer self-update

# Mise en place de l’application
# Récupération des sources
WORKDIR /var/www/html
RUN git clone https://gitlab.com/siollb/NetworkManager.git

# Modification de l’utilisateur et du groupe propriétaire :
RUN chown -R www-data:www-data NetworkManager

# Installation des dépendances
WORKDIR /var/www/html/NetworkManager
RUN composer i

# Copie du fichier des variables d'environnement
COPY .env .

# Configuration du VirtualHost
COPY default-ssl.conf /etc/apache2/sites-available/

# Modules et configuration Apache à activer
RUN a2enmod ssl \
 && a2enmod rewrite \
 && a2ensite default-ssl

EXPOSE 443

CMD ["/usr/sbin/apache2ctl","-D","FOREGROUND"]
