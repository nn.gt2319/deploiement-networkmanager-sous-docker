# Informations 

## Caractéristiques

Nom : servDockerFS
Utilisation du template (présent sur nfscub) debian12_base_2023.tar.gz
Mémoire : 1 Go
Réseau Infrasys

## INSTALLATION DE DOCKER

 ``` 
 apt update 
 apt install ca-certificates curl gnupg
 curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
 chmod a+r /etc/apt/keyrings/docker.gpg
 ``` 

 Intégration du dépôt docker dans le fichier source.list et mise à jour des dépôts

 ``` 
 echo \
"deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg]
https://download.docker.com/linux/debian \
"$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
 ```
Installation de Docker
 ```
 apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
 ```

#### Connexion à l’application (client)

URL : https://192.168.60.35:44344
Le compte pour se connecter est le suivant :
Login : admin@cub.corsica
Mdp : @dminNM